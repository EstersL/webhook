package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"webhook/model"
)

var robot = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=xxx"

func SendMessage(wechatMessage *model.WechatMessage)(err error){
	message, err := json.Marshal(wechatMessage)
	if err != nil {
		return
	}
	if robot == "" {
		err = fmt.Errorf("robot为空")
		return
	}

	requst, err := http.NewRequest(
		"POST",
		robot,
		bytes.NewBuffer(message))

	if err != nil {
		return
	}
	requst.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(requst)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	fmt.Println("response Status:", resp.Status)
	//fmt.Println("response Headers:", resp.Header)

	return
}