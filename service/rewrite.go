package service

import (
	"bytes"
	"fmt"
	"strconv"
	"time"
	"webhook/model"
)

// 告警转换为MarkDown
func Rewrite(alertInfo *model.AlertInfo)(wechatMessage *model.WechatMessage,err error){
	var buffer bytes.Buffer
	Timestamp,err := strconv.ParseInt(alertInfo.Timestamp,10, 64)
	tm := time.Unix(Timestamp, 0)
	if err != nil{
		return
	}
	buffer.WriteString(fmt.Sprintf("### 云监控告警 当前状态:%s \n", alertInfo.AlertState))
	buffer.WriteString(fmt.Sprintf("\n>告警级别: %s\n", alertInfo.TriggerLevel))
	buffer.WriteString(fmt.Sprintf("\n>告警类型: %s\n", alertInfo.AlertName))
	buffer.WriteString(fmt.Sprintf("\n>告警详情: %s %s (当前值：%s)\n", alertInfo.MetricName,alertInfo.Expression,alertInfo.CurValue))
	buffer.WriteString(fmt.Sprintf("\n>故障实例: %s %s\n", alertInfo.MetricProject,alertInfo.Dimensions))
	buffer.WriteString(fmt.Sprintf("\n> 触发时间: %s\n", tm.Format("2006-01-02 15:04:05")))

	wechatMessage = &model.WechatMessage{
		MsgType: "markdown",
		Markdown: &model.Markdown{
			Content:  buffer.String(),
		},
	}
	return
}
