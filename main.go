package main

import (
	"github.com/gin-gonic/gin"
	"webhook/controller"
)

func main(){
	r := gin.Default()
	r.POST("/webhook",controller.Webhook)
	r.Run(":777")
}