package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"webhook/model"
	"webhook/service"
)

func Webhook(c *gin.Context)(){
		var alertInfo *model.AlertInfo
		c.ShouldBind(&alertInfo)
		for k, v := range c.Request.PostForm {
			fmt.Printf("k:%v\n", k)
			fmt.Printf("v:%v\n", v)
		}
		// 格式转换
		wechatMessage,err:=service.Rewrite(alertInfo)
		if err != nil{
			panic(err)
			return
		}
		// 发送消息
		err = service.SendMessage(wechatMessage)
		if err != nil{
			panic(err)
			return
		}
		c.Writer.WriteString("SendMessage success\n")
}