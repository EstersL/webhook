package model

type AlertInfo struct {
	AlertName string `form:"alertName"` // 报警名称。
	AlertState    string  `form:"alertState"` // 报警状态。根据实际情况返回以下三种状态中的一种： OK ALERT INSUFFICIENT_DATA
	CurValue    string  `form:"curValue"` // 报警发生或恢复时监控项的当前值。
	Dimensions    string  `form:"dimensions"` // 发生报警的对象。示例： userId = 12****, instanceId = i-12****
	Expression    string `form:"expression"` //  报警条件。
	InstanceName    string `form:"instanceName"`  // 实例名称。
	MetricName    string `form:"metricName"`  // 监控项名称。
	MetricProject    string `form:"metricProject"`  // 产品名称。监控项和产品名称请参见预设监控项参考。
	Namespace    string  `form:"namespace"` // 产品的命名空间。与metricProject相同。
	PreTriggerLevel    string `form:"preTriggerLevel"` //  上一次触发报警的级别。
	RuleId    string  `form:"ruleId"` // 触发此次报警的报警规则ID。
	Timestamp    string `form:"timestamp"` //  发生报警的时间戳。
	TriggerLevel    string `form:"triggerLevel"` //  本次触发报警的级别。
	UserId    string `form:"userId"` //  用户ID。
}