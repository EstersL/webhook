package model

type WechatMessage struct{
	MsgType string `json:"msgtype"`
	Markdown *Markdown `json:"markdown"`
}

type Markdown struct{
	Content string `json:"content"`
}